"""
Old School RuneScape farming/TangleRoot simulator
Created by RenderScape
Values used are from public information on the OSRS wiki
This is NOT associated to Jagex in ANY WAY
"""

level_to_xp = {1: 0,
               2: 83,
               3: 91,
               4: 102,
               5: 112,
               6: 124,
               7: 138,
               8: 151,
               9: 168,
               10: 185,
               11: 204,
               12: 226,
               13: 249,
               14: 274,
               15: 304,
               16: 335,
               17: 369,
               18: 408,
               19: 450,
               20: 497,
               21: 548,
               22: 606,
               23: 667,
               24: 737,
               25: 814,
               26: 898,
               27: 990,
               28: 1094,
               29: 1207,
               30: 1332,
               31: 1470,
               32: 1623,
               33: 1791,
               34: 1977,
               35: 2182,
               36: 2409,
               37: 2658,
               38: 2935,
               39: 3240,
               40: 3576,
               41: 3947,
               42: 4358,
               43: 4810,
               44: 5310,
               45: 5863,
               46: 6471,
               47: 7144,
               48: 7887,
               49: 8707,
               50: 9612,
               51: 10612,
               52: 11715,
               53: 12934,
               54: 14278,
               55: 15764,
               56: 17404,
               57: 19214,
               58: 21212,
               59: 23420,
               60: 25856,
               61: 28546,
               62: 31516,
               63: 34795,
               64: 38416,
               65: 42413,
               66: 46826,
               67: 51699,
               68: 57079,
               69: 63019,
               70: 69576,
               71: 76818,
               72: 84812,
               73: 93638,
               74: 103383,
               75: 114143,
               76: 126022,
               77: 139138,
               78: 153619,
               79: 169608,
               80: 187260,
               81: 206750,
               82: 228269,
               83: 252027,
               84: 278259,
               85: 307221,
               86: 339198,
               87: 374502,
               88: 413482,
               89: 456519,
               90: 504037,
               91: 556499,
               92: 614422,
               93: 678376,
               94: 748985,
               95: 826944,
               96: 913019,
               97: 1008052,
               98: 1112977,
               99: 1228825,
               100: 186965569}  # "level 100" is 200m xp in this sim

crop_types = {
    'allotment': {
        'plantable_crops': {
            'potato': {'plant_xp': 8,
                       'harvest_xp': 9,
                       'base_chance': 281040,
                       'level': 1},
            'onion': {'plant_xp': 9.5,
                      'harvest_xp': 10.5,
                      'base_chance': 281040,
                      'level': 5},
            'cabbage': {'plant_xp': 10,
                        'harvest_xp': 11.5,
                        'base_chance': 281040,
                        'level': 7},
            'tomato': {'plant_xp': 12.5,
                       'harvest_xp': 14,
                       'base_chance': 281040,
                       'level': 12},
            'sweetcorn': {'plant_xp': 17,
                          'harvest_xp': 19,
                          'base_chance': 224832,
                          'level': 20},
            'strawberry': {'plant_xp': 26,
                           'harvest_xp': 29,
                           'base_chance': 187360,
                           'level': 31},
            'watermelon': {'plant_xp': 48.5,
                           'harvest_xp': 54.5,
                           'base_chance': 160594,
                           'level': 47},
            'snape_grass': {'plant_xp': 82,
                            'harvest_xp': 82,
                            'base_chance': 173977,
                            'level': 61},
        },
        'harvest_amount': 10,
        'total_patches': 15,
        'use_compost': True,
        'grow_days': 1,
    },
    'flower': {
        'plantable_crops': {
            'marigold': {'plant_xp': 8.5,
                         'harvest_xp': 47,
                         'base_chance': 281040,
                         'level': 2},
            'rosemary': {'plant_xp': 12,
                         'harvest_xp': 66.5,
                         'base_chance': 281040,
                         'level': 11},
            'nasturtium': {'plant_xp': 19.5,
                           'harvest_xp': 111,
                           'base_chance': 281040,
                           'level': 24},
            'woad': {'plant_xp': 20.5,
                     'harvest_xp': 115.5,
                     'base_chance': 281040,
                     'level': 25},
            'limpwurt': {'plant_xp': 21.5,
                         'harvest_xp': 120,
                         'base_chance': 224832,
                         'level': 26},
            'white_lily': {'plant_xp': 42,
                           'harvest_xp': 250,
                           'base_chance': 281040,
                           'level': 58},
        },
        'harvest_amount': 1,
        'total_patches': 7,
        'use_compost': True,
        'grow_days': 1,
    },
    'herb': {
        'plantable_crops': {
            'guam': {'plant_xp': 11,
                     'harvest_xp': 12.5,
                     'base_chance': 98364,
                     'level': 9},
            'marrentill': {'plant_xp': 13.5,
                           'harvest_xp': 15,
                           'base_chance': 98364,
                           'level': 14},
            'tarromin': {'plant_xp': 16,
                         'harvest_xp': 18,
                         'base_chance': 98364,
                         'level': 19},
            'harralander': {'plant_xp': 21.5,
                            'harvest_xp': 24,
                            'base_chance': 98364,
                            'level': 26},
            'goutweed': {'plant_xp': 105,
                         'harvest_xp': 45,
                         'base_chance': 98364,
                         'level': 29},
            'ranarr': {'plant_xp': 27,
                       'harvest_xp': 30.5,
                       'base_chance': 98364,
                       'level': 32},
            'toadflax': {'plant_xp': 34,
                         'harvest_xp': 38.5,
                         'base_chance': 98364,
                         'level': 38},
            'irit': {'plant_xp': 43,
                     'harvest_xp': 48.5,
                     'base_chance': 98364,
                     'level': 44},
            'avantoe': {'plant_xp': 54.5,
                        'harvest_xp': 61.5,
                        'base_chance': 98364,
                        'level': 50},
            'kwuarm': {'plant_xp': 69,
                       'harvest_xp': 78,
                       'base_chance': 98364,
                       'level': 56},
            'snapdragon': {'plant_xp': 87.5,
                           'harvest_xp': 98.5,
                           'base_chance': 98364,
                           'level': 62},
            'cadantine': {'plant_xp': 106.5,
                          'harvest_xp': 120,
                          'base_chance': 98364,
                          'level': 67},
            'lantadyme': {'plant_xp': 134.5,
                          'harvest_xp': 151.5,
                          'base_chance': 98364,
                          'level': 73},
            'dwarf_weed': {'plant_xp': 170.5,
                           'harvest_xp': 192,
                           'base_chance': 98364,
                           'level': 79},
            'torstol': {'plant_xp': 199.5,
                        'harvest_xp': 224.5,
                        'base_chance': 98364,
                        'level': 85}
        },
        'harvest_amount': 8,
        'total_patches': 9,
        'use_compost': True,
        'grow_days': 1,
    },
    'tree': {
        'plantable_crops': {
            'oak': {'plant_xp': 481.3,
                    'harvest_xp': 0,
                    'base_chance': 22483,
                    'level': 15},
            'willow': {'plant_xp': 1481.5,
                       'harvest_xp': 0,
                       'base_chance': 16059,
                       'level': 30},
            'maple': {'plant_xp': 3448.4,
                      'harvest_xp': 0,
                      'base_chance': 14052,
                      'level': 45},
            'yew': {'plant_xp': 7150.9,
                    'harvest_xp': 0,
                    'base_chance': 11242,
                    'level': 60},
            'magic': {'plant_xp': 13913.8,
                      'harvest_xp': 0,
                      'base_chance': 9368,
                      'level': 75}
        },
        'harvest_amount': 0,
        'total_patches': 6,
        'use_compost': False,
        'grow_days': 1,
    },
    'fruit_tree': {
        'plantable_crops': {
            'apple': {'plant_xp': 1221.5,
                      'harvest_xp': 8.5,
                      'base_chance': 9000,
                      'level': 27},
            'banana': {'plant_xp': 1778.5,
                       'harvest_xp': 10.5,
                       'base_chance': 9000,
                       'level': 33},
            'orange': {'plant_xp': 2505.7,
                       'harvest_xp': 13.5,
                       'base_chance': 9000,
                       'level': 39},
            'curry': {'plant_xp': 2946.9,
                      'harvest_xp': 15,
                      'base_chance': 9000,
                      'level': 42},
            'pineapple': {'plant_xp': 4662,
                          'harvest_xp': 21.5,
                          'base_chance': 9000,
                          'level': 51},
            'papaya': {'plant_xp': 6218.6,
                       'harvest_xp': 27,
                       'base_chance': 9000,
                       'level': 57},
            'palm': {'plant_xp': 10260.6,
                     'harvest_xp': 41.5,
                     'base_chance': 9000,
                     'level': 68},
            'dragonfruit': {'plant_xp': 17475,
                            'harvest_xp': 70,
                            'base_chance': 9000,
                            'level': 81}
        },
        'harvest_amount': 6,
        'total_patches': 6,
        'use_compost': False,
        'grow_days': 1,
    },
    'bush': {
        'plantable_crops': {
            'redberry': {'plant_xp': 75.5,
                         'harvest_xp': 4.5,
                         'base_chance': 44966,
                         'level': 10},
            'cadavaberry': {'plant_xp': 120.5,
                            'harvest_xp': 7,
                            'base_chance': 37472,
                            'level': 22},
            'dwellberry': {'plant_xp': 209,
                           'harvest_xp': 12,
                           'base_chance': 32119,
                           'level': 36},
            'jangerberry': {'plant_xp': 335,
                            'harvest_xp': 19,
                            'base_chance': 28104,
                            'level': 48},
            'whiteberry': {'plant_xp': 515.5,
                           'harvest_xp': 29,
                           'base_chance': 28104,
                           'level': 59},
            'poison_ivy': {'plant_xp': 795,
                           'harvest_xp': 45,
                           'base_chance': 28104,
                           'level': 70}
        },
        'harvest_amount': 15,
        'total_patches': 5,
        'use_compost': False,
        'grow_days': 1,
    },
    'hops':
        {
            'plantable_crops': {
                'barley': {'plant_xp': 8.5,
                           'harvest_xp': 9.5,
                           'base_chance': 112416,
                           'level': 3},
                'hammerstone': {'plant_xp': 9,
                                'harvest_xp': 10,
                                'base_chance': 112416,
                                'level': 4},
                'asgarnian': {'plant_xp': 10.9,
                              'harvest_xp': 12,
                              'base_chance': 89933,
                              'level': 8},
                'jute': {'plant_xp': 13,
                         'harvest_xp': 14.5,
                         'base_chance': 89933,
                         'level': 13},
                'yanillian': {'plant_xp': 14.5,
                              'harvest_xp': 16,
                              'base_chance': 74944,
                              'level': 16},
                'krandorian': {'plant_xp': 17.5,
                               'harvest_xp': 19.5,
                               'base_chance': 64238,
                               'level': 21},
                'wildblood': {'plant_xp': 23,
                              'harvest_xp': 26,
                              'base_chance': 56208,
                              'level': 28},
            },
            'harvest_amount': 20,
            'total_patches': 4,
            'use_compost': True,
            'grow_days': 1,
        },
    'cactus':
        {
            'plantable_crops': {
                'cactus': {'plant_xp': 440.5,
                           'harvest_xp': 25,
                           'base_chance': 7000,
                           'level': 55},
                'potato_cactus': {'plant_xp': 298,
                                  'harvest_xp': 68,
                                  'base_chance': 160594,
                                  'level': 64},
            },
            'harvest_amount': 10,
            'total_patches': 2,
            'use_compost': False,
            'grow_days': 1,
        },
    'belladonna': {
        'plantable_crops': {
            'nightshade': {
                'plant_xp': 603,
                'harvest_xp': 0,
                'base_chance': 8000,
                'level': 63
            },
        },
        'harvest_amount': 0,
        'total_patches': 1,
        'use_compost': True,
        'grow_days': 1,
    },
    'mushroom': {
        'plantable_crops': {
            'mushroom': {
                'plant_xp': 61.5,
                'harvest_xp': 57.7,
                'base_chance': 7500,
                'level': 53
            },
        },
        'harvest_amount': 6,
        'total_patches': 1,
        'use_compost': True,
        'grow_days': 1,
    },
    'seaweed': {
        'plantable_crops': {
            'giant_seaweed': {
                'plant_xp': 19,
                'harvest_xp': 21,
                'base_chance': 7500,
                'level': 23
            }
        },
        'harvest_amount': 20,
        'total_patches': 2,
        'use_compost': True,
        'grow_days': 1,
    },
    'grapevine': {
        'plantable_crops': {
            'grape': {
                'plant_xp': 656.5,
                'harvest_xp': 40,
                'base_chance': 385426,
                'level': 36
            },
        },
        'harvest_amount': 10,
        'total_patches': 12,
        'use_compost': False,
        'grow_days': 1,
    },
    'hardwood': {
        'plantable_crops': {
            'teak': {
                'plant_xp': 7325,
                'harvest_xp': 0,
                'base_chance': 5000,
                'level': 35
            },
            'mahogany': {
                'plant_xp': 15788,
                'harvest_xp': 0,
                'base_chance': 5000,
                'level': 55
            },
        },
        'harvest_amount': 0,
        'total_patches': 3,
        'use_compost': False,
        'grow_days': 4,
    },
    'calquat': {
        'plantable_crops': {
            'calquat': {
                'plant_xp': 12225.5,
                'harvest_xp': 48.5,
                'base_chance': 6000,
                'level': 72
            },
        },
        'harvest_amount': 6,
        'total_patches': 1,
        'use_compost': False,
        'grow_days': 1,
    },
    'celastrus': {
        'plantable_crops': {
            'celastrus': {
                'plant_xp': 14330,
                'harvest_xp': 23.5,
                'base_chance': 9000,
                'level': 85
            },
        },
        'harvest_amount': 6,
        'total_patches': 1,
        'use_compost': True,
        'grow_days': 1,
    },
    'crystal': {
        'plantable_crops': {
            'crystal_tree': {
                'plant_xp': 13666,
                'harvest_xp': 0,
                'base_chance': 9000,
                'level': 74
            }
        },
        'harvest_amount': 0,
        'total_patches': 1,
        'use_compost': False,
        'grow_days': 1,
    },
    'hespori': {
        'plantable_crops': {
            'hespori': {
                'plant_xp': 12662,
                'harvest_xp': 0,
                'base_chance': 7000,
                'level': 65
            }
        },
        'harvest_amount': 0,
        'total_patches': 1,
        'use_compost': False,
        'grow_days': 2,
    },
    'redwood': {
        'plantable_crops': {
            'redwood': {
                'plant_xp': 22680,
                'harvest_xp': 0,
                'base_chance': 5000,
                'level': 90
            }
        },
        'harvest_amount': 0,
        'total_patches': 1,
        'use_compost': False,
        'grow_days': 5,
    },
    'spirit_tree': {
        'plantable_crops': {
            'spirit_tree': {
                'plant_xp': 19500.5,
                'harvest_xp': 0,
                'base_chance': 5000,
                'level': 83
            },
        },
        'harvest_amount': 0,
        'total_patches': 5,
        'use_compost': False,
        'grow_days': 3,
    }
}

ultracompost_xp = 36


def build_crop_type_map():
    number_to_crop_type = {}
    counter = 1

    for key in crop_types:
        number_to_crop_type[counter] = key
        counter += 1

    return number_to_crop_type


def build_crop_map(crop_type):
    number_to_crop_map = {}
    counter = 1

    for key in crop_types[crop_type]['plantable_crops']:
        if crop_types[crop_type]['plantable_crops'][key]['level'] <= player.current_level:
            number_to_crop_map[counter] = key
            counter += 1
    return number_to_crop_map


def build_crop_type_map_harvest_amount():
    number_to_crop_type = {}
    counter = 1

    for key in crop_types:
        if crop_types[key]['harvest_amount'] != 0:
            number_to_crop_type[counter] = key
            counter += 1

    return number_to_crop_type


class Player:
    def __init__(self, start_level):
        self.current_level = start_level
        self.xp_to_next_level = level_to_xp[start_level + 1]
        self.chance_not_get_pet = 0
        self.current_farm_run = {k: [] for k in crop_types}
        self.grow_timers = {k: crop_types[k]['grow_days'] for k in crop_types}
        self.days_farmed = 0

    def change_farm_run(self, patch_type, command, args):
        if command == 'CLEAR':
            # Clear all crops of this type
            self.current_farm_run[patch_type] = []
        elif command == 'CHANGE_CROP':
            # first command is new crop, second is number
            self.current_farm_run[patch_type] = args
            self.grow_timers[patch_type] = crop_types[patch_type]['grow_days']

    def do_farm_run(self):
        total_xp = 0
        total_crops = 0
        for patch_type in self.current_farm_run:
            self.grow_timers[patch_type] -= 1
            if len(self.current_farm_run[patch_type]) > 0:  # if anything planted in this type of patch
                if self.grow_timers[patch_type] <= 0:  # patch has grown
                    self.grow_timers[patch_type] = crop_types[patch_type]['grow_days']
                    planted_crop = self.current_farm_run[patch_type][0]
                    planted_amount = self.current_farm_run[patch_type][1]

                    xp_gained = crop_types[patch_type]['plantable_crops'][planted_crop]['plant_xp'] + \
                                (crop_types[patch_type]['plantable_crops'][planted_crop]['harvest_xp'] *
                                 crop_types[patch_type]['harvest_amount'])

                    if crop_types[patch_type]['use_compost']:
                        xp_gained += ultracompost_xp

                    pet_base_chance = crop_types[patch_type]['plantable_crops'][planted_crop]['base_chance']

                    for i in range(planted_amount):
                        total_xp += xp_gained
                        total_crops += 1
                        chance_not_get_pet = 1 - (1 / (pet_base_chance - (25 * self.current_level)))
                        if self.chance_not_get_pet == 0:
                            self.chance_not_get_pet = chance_not_get_pet
                        else:
                            self.chance_not_get_pet *= chance_not_get_pet

                        self.xp_to_next_level -= xp_gained
                        while self.xp_to_next_level <= 0:
                            self.current_level += 1
                            self.xp_to_next_level += level_to_xp[self.current_level + 1]
                else:
                    print(f'Patch: {patch_type} has not yet grown. It has {self.grow_timers[patch_type]} days left')

        self.days_farmed += 1
        print(f'You harvested {total_crops} total patches and gained {total_xp} xp\n')


print('Welcome to RuneScape farming simulator 2024')
print('Created by RenderScape')
print('ASSUMPTIONS:\n'
      'Plants never get diseased or die.\n'
      'Ultracompost is always used on allotments, flowers, herbs, hops, seaweed, celastrus, belladona.\n'
      'It is never used on all other patches.\n'
      'Other farming boosts like secateurs are not used.\n'
      f'Ultracompost is bought from the G.E and gives {ultracompost_xp}xp per patch.\n')
player_start_level = int(input('Input starting farming level: '))
player = Player(player_start_level)
done = False

while not done:
    print(f'\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n'
          f'Your current daily farm run is:\n'
          f'{player.current_farm_run}\n'
          f'You harvest this amount of produce from each crop:\n'
          f'Allotment: {crop_types["allotment"]["harvest_amount"]}\n'
          f'Herbs: {crop_types["herb"]["harvest_amount"]}\n'
          f'Bushes: {crop_types["bush"]["harvest_amount"]}\n'
          f'Hops: {crop_types["hops"]["harvest_amount"]}\n'
          f'Cactus: {crop_types["cactus"]["harvest_amount"]}\n'
          f'Seaweed: {crop_types["seaweed"]["harvest_amount"]}\n'
          f'Grape: {crop_types["grapevine"]["harvest_amount"]}\n'
          f'Celastrus: {crop_types["celastrus"]["harvest_amount"]}\n'
          f'Your current farming level is: {player.current_level}\n'
          f'You need {player.xp_to_next_level:.1f} xp to get to level {player.current_level + 1}\n'
          f'You have farmed for {player.days_farmed} days\n')

    if player.chance_not_get_pet != 0:
        print(
            f'You have had a total chance of {(1 - player.chance_not_get_pet) * 100}% of getting the Tangleroot pet.\n')

    try:
        main_menu_option = int(input('Select an action:\n'
                                     '1. Change farm run.\n'
                                     '2. Change harvest amounts.\n'
                                     '3. Do a farm run.\n'
                                     '0. Exit program.\n'))
    except ValueError:
        print('Invalid option, please input a number from the list.')
        continue

    if main_menu_option == 0:
        done = True
    elif main_menu_option == 1:
        number_to_type = build_crop_type_map()
        crop_to_change = int(input('Select a crop type to change:\n'
                                   f'{number_to_type}\n'))
        crop_type = number_to_type[crop_to_change]

        number_to_command = {1: 'CHANGE_CROP',
                             2: 'CLEAR'}
        command_number = int(input('Select a change to make:\n'
                                   '1. Change crop type or number\n'
                                   '2. Clear all crops of type\n'))
        command = number_to_command[command_number]
        if command == 'CHANGE_CROP':
            number_to_crop = build_crop_map(crop_type)
            if len(number_to_crop) == 0:
                print('No crops available for this patch at this level!')
                continue
            else:
                new_crop_type_number = int(input('Choose a new crop to plant:\n'
                                                 f'{number_to_crop}\n'))
                seed_to_plant = number_to_crop[new_crop_type_number]
                new_crop_number = int(input('Choose number of patches of this crop type to harvest each day\n'
                                            f'(This crop type has {crop_types[crop_type]["total_patches"]} total patches)\n'))
                args = [seed_to_plant, new_crop_number]
        elif command == 'CLEAR':
            args = None

        player.change_farm_run(crop_type, command, args)
    elif main_menu_option == 2:
        number_to_type = build_crop_type_map_harvest_amount()
        crop_to_change = int(input('Select a crop type to change harvest amount of:\n'
                                   f'{number_to_type}\n'))
        crop_type = number_to_type[crop_to_change]
        new_harvest_quantity = int(input(f'Input a new harvest quantity for ALL {crop_type} patches:\n'))
        new_harvest_quantity = max(1, new_harvest_quantity)
        crop_types[crop_type]['harvest_amount'] = new_harvest_quantity
    elif main_menu_option == 3:
        player.do_farm_run()
