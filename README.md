# TanglerootSimulator

## Instructions for non-coders on windows

1. Get Python. Python is the programming language used to create this project and is required to use this tool.  

- Download Python from here: https://www.python.org/downloads/

- Run the installer you got from the download

- IMPORTANT: Make sure you tick the box "Add python.exe to PATH"

- Allow the installer to run and wait for it to finish 

2. Download this repository, repo for short. A repo is a set of code packaged together so it can be passed around.

- Click the blue "code" button in the top right of this, and click "zip" under download

- Extract the zip

3. Run the program. This is a command-line program, so you enter commands by typing them and hitting enter.

- Inside the folder with the tangleroot.py file, type "cmd" into the address bar in your file explorer, and hit enter to open a command prompt.

- Type "python tangleroot.py" and hit enter to start the program 

- Have fun!

